#pragma once
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SpheresPackAlgorithm.generated.h"


// First FVector - matrix/grid position
// Second FVector - world position
using SpherePosition_t = TTuple<FVector, FVector>;

/**
 * This class responsible for building Sphere shaped grid of available points in space ... 
 * ... for spheres with specific radius.
 *
 * Packing type: Hexagonal Close Packing (HCP).
 */
UCLASS()
class SPHERESSHOOTER_API USpheresPackAlgorithm : public UObject
{
	GENERATED_BODY()

public:
	void Setup(uint32 tPackRadius, uint32 tSphereRadius, uint32 tSpaceBeetweenSpheres,
	           FVector tCenter = FVector::ZeroVector, uint32 tCenterRadius = 0);
	void Execute();

	TArray<SpherePosition_t> GetPackedPositions();
	// This method required, since after first wave, we take PlayerLocation to set it as ...
	// new spawn center, but we slightly shift location, to be aligned with game grid
	// Shift can be about: "0" - "SphereRadius" usits
	FVector GetSpawnCenter();

private:
	uint32 PackRadius;
	uint32 SphereRadius;
	uint32 SpaceBeetweenSpheres;
	FVector Center;
	// CenterRadius - represent blank space with form of sphere.
	// Used to prevent spawn spheres IN player, and to pack with separation (separate critical from regular spheres)
	uint32 CenterRadius;

	TArray<SpherePosition_t> PackedPositions;

	// ======================================== Hex Border Generate Positions
	// HexBorderGeneratePositionData:
	//	First int32 - X shift (multiplied by WaveRadius)
	//	First int32 - Y shift (multiplied by WaveRadius)
	//	FVector2D - direction
	using HexBorderGenPosData_t = TTuple<int32, int32, FVector2D>;
	// HexBorderGeneratePositionData
	TArray<HexBorderGenPosData_t> GenPosData;

	void PrepareHexBorderGenPosData();

	// ==================================================
	/**
	 * Generate positions for specific Z.
	 * 
	 * @param GridZ - Z coordinate in matrix/grid (not in game world)
	 */
	bool GenerateNewPositionsForZLevel(int32 GridZ, int32 StartWaveRadius);

	/**
	 * Generate positions as Hex "Wave".
	 * 
	 * @param GridZ - Z coordinate in matrix/grid (not in game world)
	 * @param WaveRadius - Radius of the wave (also wave index number)
	 */
	bool GenerateNewPositionsWave(int32 GridZ, int32 WaveRadius);

	// Hex has 6 borders, so we need to call this function 6 times for each "Wave"
	bool GeneratePositionsOnHexBorder(int32 GridX, int32 GridY, int32 GridZ, int32 WaveRadius,
	                                  const FVector2D &Direction);

	int32 GetStartWaveRadius(int32 GridZ);

	// ======================================== Create Position
	const float Cos60 = FMath::Cos(FMath::DegreesToRadians(60));
	const float Sin60 = FMath::Sin(FMath::DegreesToRadians(60));
	float SphereDiameter;
	// PosShiftZ is Apothem - triangle center height from each side (for this type of triangle)
	float PosShiftZ;
	float PosShiftX; // Shift for compact positioning
	float EvenZPosShiftY; // -//-
	float OddZPosShiftY; // -//-
	// Note: above saved values used by each point (to save processor resources)

	void AddPosition(int32 GridX, int32 GridY, int32 GridZ, FVector WorldPos);
	FVector CreatePosition(int32 GridX, int32 GridY, int32 GridZ);
	bool PositionInBounds(const FVector &Pos, int32 GridZ);

	// Get Center Radius At specific height.
	// Since we are going UP/DOWN (making waves) along the sphere, it's radius will ...
	// ... decrease/increase and that will affect "at which radius" we need to start wave
	float GetCenterRadiusAtZ(int32 GridZ) const;

	//
	FVector GridToWorldPosition(FVector GridPos) const;
	FVector WorldToGridPosition(FVector WorldPos) const;

	// ======================================== Align sphere positions between different waves
	bool FirstRun = true;
	FVector OriginCenter;
	// GridShift - contain Center shift (for waves after first) from the OriginCenter (first wave)
	FVector GridShift;

	// Set shift in grid (for waves after first)
	void SetShiftRelativeToOrigin();
};
