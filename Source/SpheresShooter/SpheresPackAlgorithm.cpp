#include "SpheresPackAlgorithm.h"
#include "Kismet/KismetMathLibrary.h"
#include "Templates/Tuple.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
void USpheresPackAlgorithm::Setup(uint32 tPackRadius, uint32 tSphereRadius, uint32 tSpaceBeetweenSpheres,
                                  FVector tCenter, uint32 tCenterRadius)
{
	//
	PackRadius = tPackRadius;
	CenterRadius = tCenterRadius;
	Center = tCenter;

	//
	UE_LOG(LogTemp, Warning,
	       TEXT("[USpheresPackAlgorithm]: Setup| PackRadius: %d, SphereRadius: %d, CenterRadius:%d, Center:%s"),
	       PackRadius, SphereRadius, CenterRadius, *(Center.ToString()));

	if (!FirstRun)
	{
		PackedPositions.Empty(PackedPositions.Num());
		SetShiftRelativeToOrigin();
		return;
	}
	FirstRun = false;

	// ======================================== Set Grid parameters (on first run)
	PrepareHexBorderGenPosData();

	// Note: parameters below will be saved on first algorithm call, to build sphere positions ...
	// ... grid, and will be used in next game waves.
	// This parameters will not be changed till the end of game session
	SpaceBeetweenSpheres = tSpaceBeetweenSpheres;
	SphereRadius = tSphereRadius;
	OriginCenter = Center;
	UE_LOG(LogTemp, Warning, TEXT("Origin: %s"), *(OriginCenter.ToString()));

	//
	SphereDiameter = tSphereRadius * 2.f + SpaceBeetweenSpheres;
	// Apothem = SphereDiameter / 3 * FMath::Sqrt(6);
	PosShiftZ = SphereDiameter / 3.f * FMath::Sqrt(6.f);
	PosShiftX = Cos60 * SphereDiameter;
	EvenZPosShiftY = Sin60 * SphereDiameter;
	OddZPosShiftY = SphereDiameter - (Sin60 * (SphereRadius + SpaceBeetweenSpheres));
}

void USpheresPackAlgorithm::Execute()
{
	bool MoveToNextZLevel = false;
	int32 GridZ = GridShift.Z; // probably always will be zero (since player character doesn't move vertically)

	// Move UP
	do
	{
		const int32 StartWaveRadius = GetStartWaveRadius(GridZ);
		MoveToNextZLevel = GenerateNewPositionsForZLevel(GridZ, StartWaveRadius);
		GridZ++;
	}
	while (MoveToNextZLevel);

	// Move DOWN
	// TODO: Implement in future (if it will be required)
}

TArray<SpherePosition_t> USpheresPackAlgorithm::GetPackedPositions()
{
	return PackedPositions;
}

FVector USpheresPackAlgorithm::GetSpawnCenter()
{
	return Center;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
bool USpheresPackAlgorithm::GenerateNewPositionsForZLevel(int32 GridZ, int32 WaveRadius)
{
	bool AtLeastOnePositionCreated = false;
	bool GenerateNextWave = false;

	// Set 0 position (for waves after first, it will be 0 + shift)
	if (WaveRadius == 1)
	{
		const FVector Pos = CreatePosition(GridShift.X, GridShift.Y, GridZ);
		if (PositionInBounds(Pos, GridZ))
		{
			AddPosition(GridShift.X, GridShift.Y, GridZ, Pos);
			AtLeastOnePositionCreated = true;
		}
	}

	// Spread waves
	do
	{
		GenerateNextWave = GenerateNewPositionsWave(GridZ, WaveRadius);
		WaveRadius++;

		// We need at least one position in wave to Move UP
		if (GenerateNextWave)
			AtLeastOnePositionCreated = true;
	}
	while (GenerateNextWave);

	return AtLeastOnePositionCreated;
}

bool USpheresPackAlgorithm::GenerateNewPositionsWave(int32 GridZ, int32 WaveRadius)
{
	bool AtLeastOnePositionCreated = false;

	for (const HexBorderGenPosData_t &Data : GenPosData)
	{
		const int32 GridX = Data.Get<0>() * WaveRadius + GridShift.X;
		const int32 GridY = Data.Get<1>() * WaveRadius + GridShift.Y;
		FVector2D Direction = Data.Get<2>();

		if (GeneratePositionsOnHexBorder(GridX, GridY, GridZ, WaveRadius, Direction))
		{
			// We need at least one position in wave to be generated to continue to start new wave
			AtLeastOnePositionCreated = true;
		}
	}

	return AtLeastOnePositionCreated;
}


bool USpheresPackAlgorithm::GeneratePositionsOnHexBorder(int32 GridX, int32 GridY, int32 GridZ,
                                                         int32 WaveRadius, const FVector2D &Direction)
{
	bool AtLeastOnePositionCreated = false;
	for (int32 i = 0; i < WaveRadius; GridX += Direction.X, GridY += Direction.Y, i++)
	{
		FVector Pos = CreatePosition(GridX, GridY, GridZ);
		if (PositionInBounds(Pos, GridZ))
		{
			AddPosition(GridX, GridY, GridZ, Pos);
			AtLeastOnePositionCreated = true;
		}
		/*else
			UE_LOG(LogTemp, Error, TEXT("[USpheresPackAlgorithm]: SKIPPED Pos:(%f,%f,%f)"), Pos.X, Pos.Y, Pos.Z);*/
	}

	return AtLeastOnePositionCreated;
}

int32 USpheresPackAlgorithm::GetStartWaveRadius(int32 GridZ)
{
	float CenterRadiusAtZ = GetCenterRadiusAtZ(GridZ);
	int32 Radius = 1;
	if (CenterRadiusAtZ > SphereRadius)
	{
		float fRadius = FMath::DivideAndRoundUp(CenterRadiusAtZ, SphereDiameter);
		Radius = (fRadius >= 1.f) ? (fRadius + 1) : 2;
	}

	/*UE_LOG(LogTemp, Warning, TEXT("Z: %d"), Z);
	UE_LOG(LogTemp, Warning, TEXT("CenterRadiusAtZ: %f"), CenterRadiusAtZ);
	UE_LOG(LogTemp, Warning, TEXT("fRadius: %f"), fRadius);*/
	UE_LOG(LogTemp, Warning, TEXT("StartWaveRadius: %d"), Radius);
	return Radius;
}

// ======================================== Create Position
void USpheresPackAlgorithm::AddPosition(int32 GridX, int32 GridY, int32 GridZ, FVector WorldPos)
{
	// First FVector - matrix/grid position
	// Second FVector - world position
	SpherePosition_t Pos = MakeTuple(FVector(GridX, GridY, GridZ), WorldPos);
	PackedPositions.Add(Pos);
}

// Note: For X and Y axis I used standart 2D space, but shifts and scale below converts it to Hex space
// Note 2: calculation of correct positioning elements on Z axis done using Tetraeder formulas 
// https://de.wikipedia.org/wiki/Tetraeder
FVector USpheresPackAlgorithm::CreatePosition(int32 GridX, int32 GridY, int32 GridZ)
{
	FVector GridPos(GridX * 1.f, GridY * 1.f, GridZ * 1.f);
	FVector WorldPos = GridToWorldPosition(GridPos);

	return WorldPos;
}

bool USpheresPackAlgorithm::PositionInBounds(const FVector &Pos, int32 GridZ)
{
	float CenterRadiusAtZ = GetCenterRadiusAtZ(GridZ);
	float Distance = FMath::Abs(FVector::Distance(Center, Pos));

	return ((Distance + SphereRadius) <= PackRadius) && ((Distance - SphereRadius) >= CenterRadiusAtZ);
	// Note: CenterRadius condition here used for:
	// 1. Prevent spawn spheres in player.
	// 2. Separate Critical points (which effect wave) from regular.
}

float USpheresPackAlgorithm::GetCenterRadiusAtZ(int32 GridZ) const
{
	float ZWorld = (FMath::Abs(GridZ) * PosShiftZ); // Convert matrix/grid Z to World Z.
	if (ZWorld > CenterRadius)
	{
		// This case can happen when CenterRadius < SphereRadius
		//return CenterRadius;
		return 0.f;
	}

	// Calculations below based on "spherical cap" formula
	// https://www.math24.net/sphere/#:~:text=S%3D%CF%80R(2h,the%20radius%20of%20the%20sphere
	float HeightAtZ = CenterRadius - ZWorld; // Get height of "spherical cap"
	float CenterRadiusAtZ = FMath::Sqrt((2 * HeightAtZ * CenterRadius) - FMath::Square(HeightAtZ));
	return CenterRadiusAtZ;
}

//
FVector USpheresPackAlgorithm::GridToWorldPosition(FVector GridPos) const
{
	FVector WorldPos{
		GridPos.X * SphereDiameter + (PosShiftX * (-1.f * GridPos.Y)) + OriginCenter.X,
		GridPos.Y * EvenZPosShiftY + OriginCenter.Y,
		GridPos.Z * PosShiftZ + OriginCenter.Z
	};
	// Note: "(-1 * Y)" - required because Y inverted (Y-- go up, Y++ go down)
	//UE_LOG(LogTemp, Warning, TEXT("[USpheresPackAlgorithm]: new Pos:(%f,%f,%f)"), Pos.X, Pos.Y, Pos.Z);

	if (int32(GridPos.Z) % 2 != 0) // If odd Z
		WorldPos.Y -= OddZPosShiftY;

	return WorldPos;
}

FVector USpheresPackAlgorithm::WorldToGridPosition(FVector WorldPos) const
{
	// ======================================== GridZ
	int32 GridZ = FMath::RoundToInt((WorldPos.Z - OriginCenter.Z) / PosShiftZ);
	// Note: that probably always will be zero (since player character doesn't move vertically)

	// ======================================== GridY
	float WorldY = WorldPos.Y;
	if (GridZ % 2 != 0) // If odd Z
		WorldY += OddZPosShiftY;

	int32 GridY = FMath::RoundToInt((WorldY - OriginCenter.Y) / EvenZPosShiftY);

	// ======================================== GridX	
	int32 GridX = FMath::RoundToInt((WorldPos.X - (PosShiftX * (-1 * GridY)) - OriginCenter.X) / SphereDiameter);

	//
	return FVector(GridX, GridY, GridZ);
}

// ======================================== Hex Border Generate Positions
// TODO: remove this (Found algorithm to forget about this condition)
void USpheresPackAlgorithm::PrepareHexBorderGenPosData()
{
	// 1 GENERATE: from TopRight -> TopLeft
	GenPosData.Add(MakeTuple(0, -1, FVector2D(-1, 0)));

	// 2 GENERATE: from TopLeft -> MiddleLeft
	GenPosData.Add(MakeTuple(-1, -1, FVector2D(0, 1))); // Iteration direction

	// 3 GENERATE: from MiddleLeft -> BottomLeft
	GenPosData.Add(MakeTuple(-1, 0, FVector2D(1, 1))); // Iteration direction

	// 4 GENERATE: from BottomLeft -> BottomRight
	GenPosData.Add(MakeTuple(0, 1, FVector2D(1, 0))); // Iteration direction

	// 5 GENERATE: from BottomRight -> MiddleRight
	GenPosData.Add(MakeTuple(1, 1, FVector2D(0, -1))); // Iteration direction

	// 6 GENERATE: from MiddleRight -> TopRight
	GenPosData.Add(MakeTuple(1, 0, FVector2D(-1, -1))); // Iteration direction
}

// ======================================== Align sphere positions between different waves
void USpheresPackAlgorithm::SetShiftRelativeToOrigin()
{
	// This will shift position (0,0,0), from which we are starting to spread waves
	GridShift = WorldToGridPosition(Center);

	// Align Center to be right in the center of grid cell (yes, lets call it cell)
	Center = GridToWorldPosition(GridShift);
	//UE_LOG(LogTemp, Warning, TEXT("Center (after shift): %s"), *(Center.ToString()));
	UE_LOG(LogTemp, Warning, TEXT("GridShift: %s"), *(GridShift.ToString()));
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
