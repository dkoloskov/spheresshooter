#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TargetSphere.generated.h"


UCLASS()
class SPHERESSHOOTER_API ATargetSphere : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATargetSphere();

	UPROPERTY(VisibleAnywhere, Category = "Custom parameters")
	UStaticMeshComponent *Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom parameters")
	UParticleSystem* ExplosionParticlesSystem;
	
	DECLARE_EVENT_OneParam(ATargetSphere, FGotShotEvent, ATargetSphere*)
	FGotShotEvent &OnGotShotEvent() { return GotShotEvent; }

	// Critical - Will killing of this sphere effect wave or not
	bool Critical = false;
	FVector GridPosition; // Sphere position in game matrix/grid (not World position)
	
	void GotShot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/** Broadcasts whenever the layer changes */
	FGotShotEvent GotShotEvent;
};
