#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "SpheresSpawner.generated.h"


UCLASS()
class SPHERESSHOOTER_API ASpheresSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpheresSpawner();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Custom parameters")
	class UWaveData *WaveData;

	// ================================================== Debug
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;
#endif

	// ======================================== Common
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float SpawnRadius = 2000.f;

	// Only spheres destroyed in CriticalSpawnRadius will trigger end of wave
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float CriticalSpawnRadius = 1500.f;

	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float SpheresSpawnPerWave = 15.f;

	// Min value, actual number of Critical Spheres in range: CriticalSpheresSpawnPerWave - SpheresSpawnPerWave
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float CriticalSpheresSpawnPerWave = 10.f;

	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float MinimalDistanceBetweenSpheres = 80.f;

	// + this % with each new wave
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float SpheresAmountGrowthPerWave = 10;

	// + this % with each new wave (for spawn and critical radius)
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	float SpawnRadiusGrowthPerWave = 5;

	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	TSubclassOf<class ATargetSphere> RegularSphereClass;

	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	TSubclassOf<class ATargetSphere> CriticalSphereClass;

	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	uint32 SphereRadius = 100;

	// Decrease sphere radius by this value after each wave
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	uint32 DecreaseSphereRadiusPerWave = 20;

	// Decrease sphere radius by this value after each wave
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	uint32 MinimalSphereRadius = 20;

	uint32 OriginalSphereRadius; // SphereRadius on game start

	UPROPERTY(VisibleAnywhere, Category = "Custom parameters")
	bool MoveToPlayer = true;

	//
	float PlayerRadius;

	//
	UPROPERTY()
	class USpheresPackAlgorithm *PackAlgorithm;
	TArray<TTuple<FVector, FVector>> CriticalSpheresPositions;
	TArray<TTuple<FVector, FVector>> RegularSpheresPositions;
	// First FVector - matrix/grid position
	// Second FVector - world position

	// ==================================================
	void StartNextWave();
	void GenerateSpherePositions(TArray<TTuple<FVector, FVector>> &Positions, const uint32 &tSpawnRadius,
	                             const float &LocationRadius);
	void SpawnSpheres(TArray<TTuple<FVector, FVector>> &Positions, const int32 &SpheresNum, bool Critical = false);
	void SpawnSphere(TTuple<FVector, FVector> Location, bool Critical);

	// Check if we have enough available positions to spawn spheres
	// Actually this method not required if spere spawner properties aren't messed up 
	void UpdateSpheresNumWithAvailablePositions();

	//
	void OnSphereGotShot(ATargetSphere* Sphere);

	// ==================================================
	float GetSphereRadius(UStaticMeshComponent *SphereMesh);
	void SetSphereRadius(UStaticMeshComponent *SphereMesh, float Radius);

	void SavePlayerRadius();
	FVector GetPlayerLocation();
	void MoveToLocation(FVector Location);
	void UpdateSpawnRadius();

	// ======================================== Align sphere positions between different waves
	TArray<FVector> TakenPositions; // Grid positions

	// Remove taken positions
	void FilterNewPositions(TArray<TTuple<FVector, FVector>>& Positions);
	void OccupyPosition(FVector GridPos);
	void ReleasePosition(const FVector& GridPos);

	// ================================================== Debug stuff
	UPROPERTY(VisibleAnywhere, Category = "Debug stuff")
	USceneComponent *Root;

	//
	UPROPERTY(VisibleAnywhere, Category = "Debug stuff")
	UStaticMeshComponent *SpawnRadiusVisMesh;

	UPROPERTY(EditAnywhere, Category = "Debug stuff")
	bool bShowSpawnRadius = false;

	//
	UPROPERTY(VisibleAnywhere, Category = "Debug stuff")
	UStaticMeshComponent *CriticalSpawnRadiusVisMesh;

	UPROPERTY(EditAnywhere, Category = "Debug stuff")
	bool bShowCriticalSpawnRadius = false;
};
