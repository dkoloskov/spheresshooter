#include "TargetSphere.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
ATargetSphere::ATargetSphere()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
}

void ATargetSphere::GotShot()
{
	FString KillLogStr = TEXT("[ATargetSphere] GotShot()");
	KillLogStr += TEXT(" | GridPos: ") + GridPosition.ToString();
	KillLogStr += (Critical) ? TEXT(" - CRITICAL") : TEXT("");
	UE_LOG(LogTemp, Warning, TEXT("%s"), *KillLogStr);

	if (ExplosionParticlesSystem)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticlesSystem, GetActorLocation(),
			FRotator(0.f),
			true, EPSCPoolMethod::None);
		// Note: EPSCPoolMethod - Enum ParticlesSystemComponent ...
	}

	GotShotEvent.Broadcast(this);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void ATargetSphere::BeginPlay()
{
	Super::BeginPlay();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
