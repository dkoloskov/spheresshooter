#include "SpheresShooterGameMode.h"
#include "SpheresShooterHUD.h"
#include "SpheresShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/World.h"
#include "SpheresSpawner.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
ASpheresShooterGameMode::ASpheresShooterGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
		TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASpheresShooterHUD::StaticClass();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
void ASpheresShooterGameMode::BeginPlay()
{
	CreateSpawner();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void ASpheresShooterGameMode::CreateSpawner()
{
	if (!SpheresSpawnerClass)
	{
		UE_LOG(LogTemp, Error,
		       TEXT("SpheresSpawnerClass == nullptr. You should set parameter in SpheresShooterGameMode_BP"));
		return;
	}

	if (Spawner != nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Second attempt to create spawner"));
		// Don't know if this case will trigger, trying to find one critical bug
		return;
	}

	AActor *Actor = GetWorld()->SpawnActor<AActor>(SpheresSpawnerClass, FVector::ZeroVector, FRotator(0.f));
	Spawner = Cast<ASpheresSpawner>(Actor);
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
