#pragma once
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SpheresShooterGameMode.generated.h"


class ASpheresSpawner;

UCLASS(minimalapi)
class ASpheresShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpheresShooterGameMode();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Custom parameters")
	ASpheresSpawner *Spawner;


protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "Custom parameters")
	TSubclassOf<ASpheresSpawner> SpheresSpawnerClass;

	void CreateSpawner();
};
