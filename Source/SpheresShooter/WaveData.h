#pragma once
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "WaveData.generated.h"


UCLASS()
class SPHERESSHOOTER_API UWaveData : public UObject
{
	GENERATED_BODY()

public:
	FORCEINLINE void WaveNumIncr() { WaveNum++; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetWaveNum() { return WaveNum; }

	//
	FORCEINLINE void DestroyedSpheresIncr() { DestroyedSpheresNum++; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetDestroyedSpheresNum() { return DestroyedSpheresNum; }

	//
	FORCEINLINE void SetCriticalSpheresLeft(int32 value) { CriticalSpheresLeft = value; }
	FORCEINLINE void CriticalSpheresLeftDecr() { CriticalSpheresLeft--; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetCriticalSpheresLeft() { return CriticalSpheresLeft; }

	FORCEINLINE void SetRegularSpheresLeft(int32 value) { RegularSpheresLeft = value; }
	FORCEINLINE int32 GetRegularSpheresLeft() { return RegularSpheresLeft; }

private:
	int32 WaveNum = 1;
	int32 DestroyedSpheresNum = 0;

	//
	int32 CriticalSpheresLeft;
	int32 RegularSpheresLeft;
};
