#include "SpheresSpawner.h"
#include "Engine/World.h"
#include "Engine/StaticMesh.h"
#include "GameFramework/PlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "Math/BoxSphereBounds.h"
#include "Math/Sphere.h"
#include "TargetSphere.h"
#include "SpheresShooterCharacter.h"
#include "Components/CapsuleComponent.h"
#include "SpheresPackAlgorithm.h"
#include "Kismet/KismetMathLibrary.h"
#include "WaveData.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// Sets default values
ASpheresSpawner::ASpheresSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

	// ================================================== Debug stuff (Spawn Radius)
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	SpawnRadiusVisMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnRadiusVisualization"));
	SpawnRadiusVisMesh->SetupAttachment(GetRootComponent());

	CriticalSpawnRadiusVisMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CriticalSpawnRadiusVisMesh"));
	CriticalSpawnRadiusVisMesh->SetupAttachment(GetRootComponent());

	// ================================================== Debug stuff (Set Spawn Radius static mesh)
	ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere'"));
	// Note: I think it's ok to use such hardcode, since it's path to default engine assets
	if (MeshAsset.Succeeded())
	{
		SpawnRadiusVisMesh->SetStaticMesh(MeshAsset.Object);
		CriticalSpawnRadiusVisMesh->SetStaticMesh(MeshAsset.Object);
	}

	//
	UpdateSpawnRadius();
}

// ================================================== Debug stuff
#if WITH_EDITOR
void ASpheresSpawner::PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UpdateSpawnRadius();
}
#endif

// ======================================== Common
// Called every frame
void ASpheresSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
// Called when the game starts or when spawned
void ASpheresSpawner::BeginPlay()
{
	Super::BeginPlay();

	SavePlayerRadius();

	//
	OriginalSphereRadius = SphereRadius;
	if (SpheresSpawnPerWave < CriticalSpheresSpawnPerWave)
		SpheresSpawnPerWave = CriticalSpheresSpawnPerWave;
	if (SphereRadius < MinimalSphereRadius)
		SphereRadius = MinimalSphereRadius;

	//
	WaveData = NewObject<UWaveData>();
	PackAlgorithm = NewObject<USpheresPackAlgorithm>();

	StartNextWave();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void ASpheresSpawner::StartNextWave()
{
	UE_LOG(LogTemp, Warning, TEXT("===== WAVE #%d ====="), WaveData->GetWaveNum());

	// ======================================== Generate
	GenerateSpherePositions(CriticalSpheresPositions, CriticalSpawnRadius, PlayerRadius);
	UE_LOG(LogTemp, Warning, TEXT("[ASpheresSpawner]: CriticalSpheresPositions: %d"), CriticalSpheresPositions.Num());

	//
	GenerateSpherePositions(RegularSpheresPositions, SpawnRadius, CriticalSpawnRadius);
	UE_LOG(LogTemp, Warning, TEXT("[ASpheresSpawner]: RegularSpheresPositions: %d"), RegularSpheresPositions.Num());

	//
	MoveToLocation(PackAlgorithm->GetSpawnCenter());

	// ======================================== Spawn
	//WaveData->SetCriticalSpheresLeft(FMath::RandRange(CriticalSpheresSpawnPerWave, SpheresSpawnPerWave));
	WaveData->SetCriticalSpheresLeft(CriticalSpheresSpawnPerWave);
	WaveData->SetRegularSpheresLeft(SpheresSpawnPerWave - WaveData->GetCriticalSpheresLeft());
	UpdateSpheresNumWithAvailablePositions();

	//
	SpawnSpheres(CriticalSpheresPositions, WaveData->GetCriticalSpheresLeft(), true);
	UE_LOG(LogTemp, Warning, TEXT("[ASpheresSpawner]: Critical spheres spawned: %d"),
	       WaveData->GetCriticalSpheresLeft());

	//
	SpawnSpheres(RegularSpheresPositions, WaveData->GetRegularSpheresLeft());
	UE_LOG(LogTemp, Warning, TEXT("[ASpheresSpawner]: Regular spheres spawned: %d"), WaveData->GetRegularSpheresLeft());
}

void ASpheresSpawner::GenerateSpherePositions(TArray<SpherePosition_t> &Positions, const uint32 &tSpawnRadius,
                                              const float &LocationRadius)
{
	PackAlgorithm->Setup(tSpawnRadius, SphereRadius, MinimalDistanceBetweenSpheres,
	                     GetPlayerLocation(), LocationRadius);
	PackAlgorithm->Execute();

	Positions.Empty(Positions.Num());
	Positions = PackAlgorithm->GetPackedPositions();
	FilterNewPositions(Positions);
}

void ASpheresSpawner::SpawnSpheres(TArray<SpherePosition_t> &Positions, const int32 &SpheresNum, bool Critical)
{
	if (Positions.Num() == 0)
		return;

	SpherePosition_t Pos;
	int32 PosIndex;
	for (int i = 0; i < SpheresNum; i++)
	{
		PosIndex = FMath::RandRange(0, Positions.Num() - 1);
		Pos = Positions[PosIndex];
		Positions.RemoveAt(PosIndex); // prevent creating spheres at same position

		SpawnSphere(Pos, Critical);
	}
}

void ASpheresSpawner::SpawnSphere(SpherePosition_t Pos, bool Critical)
{
	if (!RegularSphereClass || !CriticalSphereClass)
	{
		UE_LOG(LogTemp, Error, TEXT("RegularSphereClass||CriticalSphereClass == nullptr (SpheresSpawner_BP)" ));
		return;
	}
	FVector &Location = Pos.Get<1>();

	// Shift for sphere (aftew first wave, when size of sphere radius decreased)
	// We can move small sphere inside space for bigger one
	if (SphereRadius < OriginalSphereRadius)
	{
		float ShiftSize = (OriginalSphereRadius - SphereRadius) / 2;
		FVector ShiftV;
		ShiftV.X += FMath::RandRange(-ShiftSize, ShiftSize);
		ShiftV.Y += FMath::RandRange(-ShiftSize, ShiftSize);
		ShiftV.Z += FMath::RandRange(-ShiftSize, ShiftSize);
		// TODO: fix shift, to make in more precise

		//UE_LOG(LogTemp, Warning, TEXT("Sphere pos shift - X:%f, Y:%f, Z:%f"), ShiftV.X, ShiftV.Y, ShiftV.Z);
		Location += ShiftV;
	}

	TSubclassOf<ATargetSphere> SphereClass = (Critical) ? CriticalSphereClass : RegularSphereClass;
	//
	AActor *Actor = GetWorld()->SpawnActor<AActor>(SphereClass, Location, FRotator(0.f));
	ATargetSphere *Sphere = Cast<ATargetSphere>(Actor);
	SetSphereRadius(Sphere->Mesh, SphereRadius);

	//
	Sphere->Critical = Critical;
	Sphere->GridPosition = Pos.Get<0>();
	Sphere->OnGotShotEvent().AddUObject(this, &ASpheresSpawner::OnSphereGotShot);
	//
	OccupyPosition(Pos.Get<0>());
}

//
void ASpheresSpawner::UpdateSpheresNumWithAvailablePositions()
{
	// Critical spheres
	int32 SpawnPositionsLeft = CriticalSpheresPositions.Num() - WaveData->GetCriticalSpheresLeft();
	if (SpawnPositionsLeft < 0)
	{
		// Prevent game from bug (when game can't be ended)
		WaveData->SetCriticalSpheresLeft(CriticalSpheresPositions.Num());
		WaveData->SetRegularSpheresLeft(SpheresSpawnPerWave - WaveData->GetCriticalSpheresLeft());

		UE_LOG(LogTemp, Error, TEXT("[SpheresSpawner] Error| No available positions to spawn Critical spheres"));
		UE_LOG(LogTemp, Error, TEXT("Not spawned critical spheres num:%d"), (-1*SpawnPositionsLeft));
		UE_LOG(LogTemp, Error,
		       TEXT(
			       "To fix: decrease CriticalSpheresSpawnPerWave OR increase CriticalSpawnRadius OR decrease SphereRadius"
		       ));
	}

	// Regular spheres
	SpawnPositionsLeft = RegularSpheresPositions.Num() - WaveData->GetRegularSpheresLeft();
	if (SpawnPositionsLeft < 0)
	{
		// Prevent game from bug (when gave can't be ended)
		WaveData->SetRegularSpheresLeft(RegularSpheresPositions.Num());

		UE_LOG(LogTemp, Error, TEXT("[SpheresSpawner] Error| No available positions to spawn Regular spheres"));
		UE_LOG(LogTemp, Error, TEXT("Not spawned regular spheres num:%d"), (-1 * SpawnPositionsLeft));
		UE_LOG(LogTemp, Error,
		       TEXT("To fix: decrease SpheresSpawnPerWave OR increase SpawnRadius OR decrease SphereRadius"));
	}
}

//
void ASpheresSpawner::OnSphereGotShot(ATargetSphere *Sphere)
{
	bool IsCritical = Sphere->Critical;
	ReleasePosition(Sphere->GridPosition);
	Sphere->Destroy();

	if (!IsCritical)
		return;

	// ======================================== IF CRITICAL (then affect wave)
	WaveData->DestroyedSpheresIncr();
	WaveData->CriticalSpheresLeftDecr();
	UE_LOG(LogTemp, Warning, TEXT("DestroyedSpheresNum: %d"), WaveData->GetDestroyedSpheresNum());
	UE_LOG(LogTemp, Warning, TEXT("CriticalSpheresLeft: %d"), WaveData->GetCriticalSpheresLeft());

	if (WaveData->GetCriticalSpheresLeft() == 0)
	{
		WaveData->WaveNumIncr();
		SpawnRadius += (SpawnRadius / 100.f) * SpawnRadiusGrowthPerWave;
		CriticalSpawnRadius += (CriticalSpawnRadius / 100.f) * SpawnRadiusGrowthPerWave;

		SpheresSpawnPerWave += (SpheresSpawnPerWave / 100.f) * SpheresAmountGrowthPerWave;
		CriticalSpheresSpawnPerWave += (CriticalSpheresSpawnPerWave / 100.f) * SpheresAmountGrowthPerWave;

		SphereRadius -= DecreaseSphereRadiusPerWave;
		if (SphereRadius < MinimalSphereRadius)
			SphereRadius = MinimalSphereRadius;

		UpdateSpawnRadius();
		StartNextWave();
	}
}

// ==================================================
float ASpheresSpawner::GetSphereRadius(UStaticMeshComponent *SphereMesh)
{
	FTransform transform; // we use NO transform, since it doesn't matter for sphere
	FBoxSphereBounds Bounds = SphereMesh->CalcBounds(transform);
	FSphere Sphere = Bounds.GetSphere();
	return Sphere.W;
}

void ASpheresSpawner::SetSphereRadius(UStaticMeshComponent *SphereMesh, float Radius)
{
	float SphereMeshRadius = GetSphereRadius(SphereMesh);
	float ScaleRatio = Radius / SphereMeshRadius;
	SphereMesh->SetRelativeScale3D(FVector(ScaleRatio));
}

void ASpheresSpawner::SavePlayerRadius()
{
	ASpheresShooterCharacter *PlayerCharacter = Cast<ASpheresShooterCharacter>(
		GetWorld()->GetFirstPlayerController()->GetPawn());

	UCapsuleComponent *PCapsule = PlayerCharacter->GetCapsuleComponent();
	PlayerRadius = PCapsule->GetScaledCapsuleHalfHeight();
	// Note: we could calculate take CapsuleHalfHeight and CapsuleRadius, but it will take\
	// additional time for SpheresPackAlgorithm improvement. No need for this now.

	UE_LOG(LogTemp, Warning, TEXT("[ASpheresSpawner]: PlayerRadius: %f"), PlayerRadius);
}


FVector ASpheresSpawner::GetPlayerLocation()
{
	ASpheresShooterCharacter *PlayerCharacter = Cast<ASpheresShooterCharacter>(
		GetWorld()->GetFirstPlayerController()->GetPawn());

	return PlayerCharacter->GetActorLocation();
}

void ASpheresSpawner::MoveToLocation(FVector Location)
{
	if (!MoveToPlayer)
		return;

	SetActorLocation(Location);
}

void ASpheresSpawner::UpdateSpawnRadius()
{
	// Spawn Radius
	if (SpawnRadiusVisMesh != nullptr)
	{
		SpawnRadiusVisMesh->SetVisibility(bShowSpawnRadius);
		if (bShowSpawnRadius)
			SetSphereRadius(SpawnRadiusVisMesh, SpawnRadius);
	}

	// Critical Spawn Radius
	if (CriticalSpawnRadiusVisMesh != nullptr)
	{
		CriticalSpawnRadiusVisMesh->SetVisibility(bShowCriticalSpawnRadius);
		if (bShowCriticalSpawnRadius)
			SetSphereRadius(CriticalSpawnRadiusVisMesh, CriticalSpawnRadius);
	}
}

// ======================================== Align sphere positions between different waves
// Note: I know that this isn't optimal solution, but it's fast in terms of implementation
// As an improvement in terms speed/performance here could be used map with ...
// ... key = "X_x_Y_y_Z_z" (where lower register actual values)

void ASpheresSpawner::FilterNewPositions(TArray<TTuple<FVector, FVector>> &Positions)
{
	for (const FVector &TakenPos : TakenPositions)
	{
		for (int i = 0; i < Positions.Num(); i++)
		{
			if (TakenPos.Equals(Positions[i].Get<0>()))
			{
				Positions.RemoveAt(i);
				break;
			}
		}
	}
}

void ASpheresSpawner::OccupyPosition(FVector GridPos)
{
	TakenPositions.Add(GridPos);
}

void ASpheresSpawner::ReleasePosition(const FVector &GridPos)
{
	TakenPositions.Remove(GridPos);
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
